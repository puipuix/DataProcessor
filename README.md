# DataProcessor

## Description

WIP

## Features

### .Core

Alternative implementation of the Entity Component System architecture where *'Entity'* and *'Component'* are regrouped as *'Data'*. 

Each **Data** (Component) is identified by a **DataId** (Entity) and can be attached to others.
**DataServer** will be your main way to use this implementation.

```cs
foreach (DataView<TimerData, DeltaData> view in DataServer.View.Been<TimerData>().With<DeltaData>().Excluding<EndedDataTag>())
{
	DataHandle<TimerData> handle = view.D1;
	ref TimerData timer = ref handle.Data;
	
	DeltaData delta = view.D2;
	timer.TimeLeft -= delta.Value;
	
	if (timer.TimeLeft < 0)
	{
		DataServer.Assign<EndedDataTag>(handle);
	}
}
```

### .ResourceLoader

Dependency injection.

```cs
[ScritableResource("KeyToJoyStick")]
public partial class KeyToJoyStickResource
{
	public float Speed;
}
```
```cs
[Autoload]
private IEnumerable<KeyToJoyStickResource> m_AllResources;
[Autoload]
private KeyToJoyStickResource m_FirstResources;

public void _Init()
{
	GDAutoLoader.LoadFor(this);
}
```

### .Utilities

#### GlobalLogger

Print the time, the file and the line of source code and to the console.

```cs
GlobalLogger.LogDebug("Starting server on port {port}, waiting {count} players...", 60000, 2);
```
```
12:23:40.601 - DBG [MyServer:30] > Starting server on port 60000, waiting 2 players...
12:23:40.649 - DBG [MyServer:40] > 	 Done.
12:23:40.651 - DBG [MyClient:48] > Connecting to 'localhost:60000' as player...
12:23:40.659 - DBG [MyClient:59] > 	 Done.
12:23:41.122 - DBG [MyClient:64] > Connected.

```

### .GodotEngine

Classes that allow you to more easily use the DataProcessor library with Godot Game Engine.

## Git

The git is divided into 3 branches:

### Stable

Stable* version of the software. No feature will be removed without it being marked as obsolete beforehand**.

### Experimental

Software version to be tested. New features not present in the stable branch should not be removed. The function corp may be altered if it is not possible to do otherwise.

### WIP

No guarantee on the stability of the version or its ability to be compiled. Features can be added or removed. Using this branch is at your own risk.

*No guarantee on a perfect stability of the software.
**Unless it conflicts with new features in Godot.